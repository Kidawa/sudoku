import Cell from './cell.js'
import Sudoku from './sudoku.js'

let sudoku = new Sudoku()

/*
 * DOM elements
 *
 */

const sudoku_div = document.querySelectorAll('#sudoku')[0]
sudoku.grid.forEach(cells => {
    const div = document.createElement("div")
    cells.forEach(cell => {
	const btn = document.createElement("button")
	div.appendChild(btn)
    })
    sudoku_div.appendChild(div)
})

const cells_btn = document.querySelectorAll('#sudoku button')
const dialog = document.querySelectorAll('dialog')[0]
const close = document.querySelectorAll('.close')[0]
const dialog_btn = document.querySelectorAll('dialog button')
const range = document.querySelectorAll('#range')[0]


/*
 * Functions
 *
 */

close.onclick = () => dialog.close()

const display = () => {
	cells_btn.forEach((btn, idx) => {
	    const cell = sudoku.cells[idx]
	    btn.innerText = cell.value
	    btn.dataset.tooltip = cell.possibleValues
	    if (cell.value) delete btn.dataset.tooltip
	})
}

const choose = cell_idx => {
    const cell = sudoku.cells[cell_idx]
    if (cell.value) return
    dialog_btn.forEach(btn => {
	const value = parseInt(btn.innerText)
	btn.disabled = !cell.possibleValues.includes(value)
	btn.onclick = () => {
	    sudoku.update(cell, value)
	    display()
	    dialog.close()
	}
    })
    dialog.showModal()
}

cells_btn.forEach((btn, idx) => {
    btn.onclick = () => choose(idx)
})

window.randomize = () => {
    const difficulty = range.value
    sudoku.randomize(difficulty)
    display()
}

//window.randomize()
