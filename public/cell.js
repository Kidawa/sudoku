export default class Cell {
	constructor() {
		this.possibleValues = [1,2,3,4,5,6,7,8,9]
	}

	get value() {
		return this.possibleValues.length === 1 ? this.possibleValues[0] : null
	}

	set value(v) {
		this.possibleValues = [v]
	}

	get entropy() {
		return this.possibleValues.length
	}

	forget(value) {
		this.possibleValues = this.possibleValues.filter(v => v !== value)
	}
}
