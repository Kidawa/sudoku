import Cell from './cell.js'

const COUNT = Object.freeze([1,2,3,4,5,6,7,8,9])
const INDEX = Object.freeze([0,1,2,3,4,5,6,7,8])

export default class Sudoku {
    constructor(seed) {
	this.grid = INDEX.map(() => INDEX.map(() => new Cell()))
    }

    get cells() {
	return this.grid.flat()
    }

    get lines() {
	return INDEX.map(i => INDEX.map(j => 
	    this.grid[Math.floor(j/3)+Math.floor(i/3)*3][i%3*3+j%3]))
    }

    get columns() {
	return INDEX.map(j => INDEX.map(i => 
	    this.grid[Math.floor(j/3)+Math.floor(i/3)*3][i%3*3+j%3]))
    }

    known() {
	return this.cells.filter(cell => cell.value).length
    }

    wave(c) {
	[this.grid, this.lines, this.columns].forEach( group => {
	    group.filter(subgroup => subgroup.includes(c)).forEach( subgroup => {
		subgroup.filter(cell => cell != c).forEach(cell => cell.forget(c.value))
	    })
	})
    }

    update(cell, value) {
	cell.value = value
	let known = this.known()
	this.wave(cell)
	let know = this.known()
	while (known < know) {
	    known = know
	    this.cells.filter(cell => cell.value).forEach(cell => this.wave(cell))
	    know = this.known()
	}
    }

    solveOnce() {
	if(this.cells.every(cell => cell.entropy < 2)) return
	let lowestEntropyCells = []
	for (var i=2; !lowestEntropyCells.length; i++) {
	    lowestEntropyCells = this.cells.filter(cell => cell.entropy == i)
	}
	const randomCell = lowestEntropyCells[Math.floor((lowestEntropyCells.length-1) * Math.random())]
	const randomValue = randomCell.possibleValues[Math.floor((randomCell.possibleValues.length-1) * Math.random())]
	this.update(randomCell, randomValue)
    }

    solve() {
	while (!this.cells.every(cell => cell.entropy < 2)) this.solveOnce()
    }

    reset() {
	this.grid = INDEX.map(() => INDEX.map(() => new Cell()))
    }

    randomize(difficulty) {
	while (!this.cells.every(cell => cell.value)) {
	    this.reset()
	    this.solve()
	}
	while (this.known() > difficulty) {
	    this.grid[Math.floor((9) * Math.random())][Math.floor((9) * Math.random())] = new Cell()
	}
	this.cells.filter(cell => cell.value).forEach(cell => this.wave(cell))
    }
}
