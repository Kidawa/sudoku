# SudoKiki

Un Un petit jeu de sudoku par kiki, gratuit sans publicité et open source 😍  
Fait avec [Pico.css](https://picocss.com)

Disponible [ici](https://kidawa.gitlab.io/sudoku/)
